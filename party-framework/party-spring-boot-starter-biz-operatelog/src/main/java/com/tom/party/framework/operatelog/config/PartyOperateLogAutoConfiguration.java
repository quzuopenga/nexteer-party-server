package com.tom.party.framework.operatelog.config;

import com.tom.party.framework.operatelog.core.aop.OperateLogAspect;
import com.tom.party.framework.operatelog.core.service.OperateLogFrameworkService;
import com.tom.party.framework.operatelog.core.service.OperateLogFrameworkServiceImpl;
import com.tom.party.module.system.api.logger.OperateLogApi;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class PartyOperateLogAutoConfiguration {

    @Bean
    public OperateLogAspect operateLogAspect() {
        return new OperateLogAspect();
    }

    @Bean
    public OperateLogFrameworkService operateLogFrameworkService(OperateLogApi operateLogApi) {
        return new OperateLogFrameworkServiceImpl(operateLogApi);
    }

}
