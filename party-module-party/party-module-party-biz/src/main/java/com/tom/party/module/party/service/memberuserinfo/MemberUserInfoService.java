package com.tom.party.module.party.service.memberuserinfo;

import java.util.*;
import javax.validation.*;
import com.tom.party.module.party.controller.admin.memberuserinfo.vo.*;
import com.tom.party.module.party.dal.dataobject.memberuserinfo.MemberUserInfoDO;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.PageParam;

/**
 * 党员信息 Service 接口
 *
 * @author tom
 */
public interface MemberUserInfoService {

    /**
     * 创建党员信息
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createMemberUserInfo(@Valid MemberUserInfoSaveReqVO createReqVO);

    /**
     * 更新党员信息
     *
     * @param updateReqVO 更新信息
     */
    void updateMemberUserInfo(@Valid MemberUserInfoSaveReqVO updateReqVO);

    /**
     * 删除党员信息
     *
     * @param id 编号
     */
    void deleteMemberUserInfo(Long id);

    /**
     * 获得党员信息
     *
     * @param id 编号
     * @return 党员信息
     */
    MemberUserInfoDO getMemberUserInfo(Long id);

    /**
     * 获得党员信息分页
     *
     * @param pageReqVO 分页查询
     * @return 党员信息分页
     */
    PageResult<MemberUserInfoDO> getMemberUserInfoPage(MemberUserInfoPageReqVO pageReqVO);

}