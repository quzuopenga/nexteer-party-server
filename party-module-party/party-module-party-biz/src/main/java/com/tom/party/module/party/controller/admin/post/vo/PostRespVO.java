package com.tom.party.module.party.controller.admin.post.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.util.*;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;
import com.alibaba.excel.annotation.*;

@Schema(description = "管理后台 - 党员组织 Response VO")
@Data
@ExcelIgnoreUnannotated
public class PostRespVO {

    @Schema(description = "id", requiredMode = Schema.RequiredMode.REQUIRED, example = "2788")
    @ExcelProperty("id")
    private Long id;

    @Schema(description = "父职务id", requiredMode = Schema.RequiredMode.REQUIRED, example = "948")
    @ExcelProperty("父职务id")
    private Long parentId;

    @Schema(description = "职位名称", requiredMode = Schema.RequiredMode.REQUIRED, example = "赵六")
    @ExcelProperty("职位名称")
    private String partyPostName;

    @Schema(description = "职位综述")
    @ExcelProperty("职位综述")
    private String partyPostDesc;

    @Schema(description = "岗位职责")
    @ExcelProperty("岗位职责")
    private Set<Long> partyResponsebilityIds;

    @Schema(description = "岗位职责中文")
    private String partyResponsebility;

    @Schema(description = "岗位人员id", example = "13240")
    @ExcelProperty("岗位人员id")
    private Long partyUserId;

    @Schema(description = "职位人员名称", example = "芋艿")
    @ExcelProperty("职位人员名称")
    private String partyUserName;

    @Schema(description = "创建时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}