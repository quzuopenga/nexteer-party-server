package com.tom.party.module.party.controller.admin.memberuserinfo.vo;

import com.tom.party.framework.desensitize.core.slider.annotation.IdCardDesensitize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDate;
import java.util.*;
import java.util.*;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;
import com.alibaba.excel.annotation.*;

@Schema(description = "管理后台 - 党员信息 Response VO")
@Data
@ExcelIgnoreUnannotated
public class MemberUserInfoRespVO {

    @Schema(description = "id", requiredMode = Schema.RequiredMode.REQUIRED, example = "21007")
    @ExcelProperty("id")
    private Long id;

    @Schema(description = "党支部", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("党支部")
    private Integer partyBranch;

    @Schema(description = "党员类别", requiredMode = Schema.RequiredMode.REQUIRED, example = "1")
    @ExcelProperty("党员类别")
    private Integer memberType;

    @Schema(description = "姓名", requiredMode = Schema.RequiredMode.REQUIRED, example = "芋艿")
    @ExcelProperty("姓名")
    private String memberName;

    @Schema(description = "性别", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("性别")
    private Integer sex;

    @Schema(description = "身份证", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("身份证")
//    @IdCardDesensitize
    private String idCard;

    @Schema(description = "民族", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("民族")
    private Integer nation;

    @Schema(description = "生日", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("生日")
    private LocalDate birthday;

    @Schema(description = "教育水平", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("教育水平")
    private Integer eduLevel;

    @Schema(description = "入党时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("入党时间")
    private LocalDate inPartyDay;

    @Schema(description = "转正时间")
    @ExcelProperty("转正时间")
    private LocalDate regularDay;

    @Schema(description = "入厂时间")
    @ExcelProperty("入厂时间")
    private LocalDate inWorkDay;

    @Schema(description = "职位id", requiredMode = Schema.RequiredMode.REQUIRED, example = "5634")
    @ExcelProperty("职位id")
    private Long partyPostId;

    @Schema(description = "职位id", requiredMode = Schema.RequiredMode.REQUIRED, example = "芋艿")
    @ExcelProperty("职位id")
    private String partyPostName;

    @Schema(description = "工作岗位", example = "芋艿")
    @ExcelProperty("工作岗位")
    private String workPostName;

    @Schema(description = "工作职级", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("工作职级")
    private Integer workPostLevel;

    @Schema(description = "所在单位", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("所在单位")
    private Integer unit;

    @Schema(description = "党员所在基地")
    @ExcelProperty("党员所在基地")
    private String habitation;

    @Schema(description = "职务类别")
    @ExcelProperty("职务类别")
    private Integer postLevel;

    @Schema(description = "党小组")
    @ExcelProperty("党小组")
    private Integer partyGroup;

    @Schema(description = "状态", example = "1")
    @ExcelProperty("状态")
    private Integer status;

    @Schema(description = "创建时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}