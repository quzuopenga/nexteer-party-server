package com.tom.party.module.party.dal.mysql.post;

import java.util.*;

import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.mybatis.core.query.LambdaQueryWrapperX;
import com.tom.party.framework.mybatis.core.mapper.BaseMapperX;
import com.tom.party.module.party.dal.dataobject.post.PartyPostDO;
import org.apache.ibatis.annotations.Mapper;
import com.tom.party.module.party.controller.admin.post.vo.*;

/**
 * 党员组织 Mapper
 *
 * @author tom
 */
@Mapper
public interface PartyPostMapper extends BaseMapperX<PartyPostDO> {

    default PageResult<PartyPostDO> selectPage(PostPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<PartyPostDO>()
                .eqIfPresent(PartyPostDO::getParentId, reqVO.getParentId())
                .likeIfPresent(PartyPostDO::getPartyPostName, reqVO.getPartyPostName())
                .eqIfPresent(PartyPostDO::getPartyPostDesc, reqVO.getPartyPostDesc())
                .eqIfPresent(PartyPostDO::getPartyResponsebilityIds, reqVO.getPartyResponsebilityIds())
                .eqIfPresent(PartyPostDO::getPartyUserId, reqVO.getPartyUserId())
                .likeIfPresent(PartyPostDO::getPartyUserName, reqVO.getPartyUserName())
                .betweenIfPresent(PartyPostDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(PartyPostDO::getId));
    }


    default List<PartyPostDO> selectByParentId(Long id) {
        return selectList(new LambdaQueryWrapperX<PartyPostDO>().eq(PartyPostDO::getParentId,id));
    }
}