package com.tom.party.module.party.dal.dataobject.post;

import com.tom.party.framework.mybatis.core.type.JsonLongSetTypeHandler;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import com.tom.party.framework.mybatis.core.dataobject.BaseDO;

/**
 * 党员组织 DO
 *
 * @author tom
 */
@TableName(value = "party_post",autoResultMap = true)
@KeySequence("party_post_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PartyPostDO extends BaseDO {

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 父职务id
     */
    private Long parentId;
    /**
     * 职位名称
     */
    private String partyPostName;
    /**
     * 职位综述
     */
    private String partyPostDesc;
    /**
     * 岗位职责
     */
    @TableField(typeHandler = JsonLongSetTypeHandler.class,value = "party_responsebility_ids")
    private Set<Long> partyResponsebilityIds;

    @TableField(exist = false)
    private String partyResponsebility;
    /**
     * 岗位人员id
     */
    private Long partyUserId;
    /**
     * 职位人员名称
     */
    private String partyUserName;

}