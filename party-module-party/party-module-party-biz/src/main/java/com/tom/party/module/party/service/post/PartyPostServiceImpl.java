package com.tom.party.module.party.service.post;

import cn.hutool.core.collection.CollUtil;
import com.tom.party.module.party.dal.mysql.post.PartyPostMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import com.tom.party.module.party.controller.admin.post.vo.*;
import com.tom.party.module.party.dal.dataobject.post.PartyPostDO;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.PageParam;
import com.tom.party.framework.common.util.object.BeanUtils;



import static com.tom.party.framework.common.exception.util.ServiceExceptionUtil.exception;
import static com.tom.party.module.party.enums.ErrorCodeConstants.*;

/**
 * 党员组织 Service 实现类
 *
 * @author tom
 */
@Service
@Validated
public class PartyPostServiceImpl implements PartyPostService {

    @Resource
    private PartyPostMapper partyPostMapper;

    @Override
    public Long createPost(PostSaveReqVO createReqVO) {
        // 插入
        PartyPostDO post = BeanUtils.toBean(createReqVO, PartyPostDO.class);
        partyPostMapper.insert(post);
        // 返回
        return post.getId();
    }

    @Override
    public void updatePost(PostSaveReqVO updateReqVO) {
        // 校验存在
        validatePostExists(updateReqVO.getId());
        // 更新
        PartyPostDO updateObj = BeanUtils.toBean(updateReqVO, PartyPostDO.class);
        partyPostMapper.updateById(updateObj);
    }

    @Override
    @Transactional
    public void deletePost(Long id) {
        // 校验存在
        validatePostExists(id);
        validatePostChildExists(id);
        // 删除
        partyPostMapper.deleteById(id);
    }

    private void validatePostChildExists(Long id) {
       List<PartyPostDO> partyPostDO =  partyPostMapper.selectByParentId(id);
       if (CollUtil.isNotEmpty(partyPostDO)) {
           throw exception(CHILD_EXISTS);
       }
    }

    private void validatePostExists(Long id) {
        if (partyPostMapper.selectById(id) == null) {
            throw exception(POST_NOT_EXISTS);
        }
    }

    @Override
    public PartyPostDO getPost(Long id) {
        return partyPostMapper.selectById(id);
    }

    @Override
    public PageResult<PartyPostDO> getPostPage(PostPageReqVO pageReqVO) {
        return partyPostMapper.selectPage(pageReqVO);
    }

    @Override
    public List<PartyPostDO> getPostList(PostPageReqVO pageReqVO) {
        System.out.println(partyPostMapper.selectPage(pageReqVO));
        return partyPostMapper.selectPage(pageReqVO).getList();
    }

}