package com.tom.party.module.party.controller.admin.post.vo;

import lombok.*;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import com.tom.party.framework.common.pojo.PageParam;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;

import static com.tom.party.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

@Schema(description = "管理后台 - 党员组织分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PostPageReqVO extends PageParam {

    @Schema(description = "父职务id", example = "948")
    private Long parentId;

    @Schema(description = "职位名称", example = "赵六")
    private String partyPostName;

    @Schema(description = "职位综述")
    private String partyPostDesc;

    @Schema(description = "岗位职责")
    private Set<Long> partyResponsebilityIds;

    @Schema(description = "岗位人员id", example = "13240")
    private Long partyUserId;

    @Schema(description = "职位人员名称", example = "芋艿")
    private String partyUserName;

    @Schema(description = "创建时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] createTime;

}