package com.tom.party.module.party.service.post;

import java.util.*;
import javax.validation.*;

import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.module.party.controller.admin.post.vo.*;
import com.tom.party.module.party.dal.dataobject.post.PartyPostDO;

/**
 * 党员组织 Service 接口
 *
 * @author tom
 */
public interface PartyPostService {

    /**
     * 创建党员组织
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createPost(@Valid PostSaveReqVO createReqVO);

    /**
     * 更新党员组织
     *
     * @param updateReqVO 更新信息
     */
    void updatePost(@Valid PostSaveReqVO updateReqVO);

    /**
     * 删除党员组织
     *
     * @param id 编号
     */
    void deletePost(Long id);

    /**
     * 获得党员组织
     *
     * @param id 编号
     * @return 党员组织
     */
    PartyPostDO getPost(Long id);

    /**
     * 获得党员组织分页
     *
     * @param pageReqVO 分页查询
     * @return 党员组织分页
     */
    PageResult<PartyPostDO> getPostPage(PostPageReqVO pageReqVO);

    List<PartyPostDO> getPostList(PostPageReqVO pageReqVO);
}