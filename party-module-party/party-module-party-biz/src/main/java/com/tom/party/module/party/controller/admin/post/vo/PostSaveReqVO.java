package com.tom.party.module.party.controller.admin.post.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 党员组织新增/修改 Request VO")
@Data
public class PostSaveReqVO {

    @Schema(description = "id", requiredMode = Schema.RequiredMode.REQUIRED, example = "2788")
    private Long id;

    @Schema(description = "父职务id", requiredMode = Schema.RequiredMode.REQUIRED, example = "948")
    @NotNull(message = "父职务id不能为空")
    private Long parentId;

    @Schema(description = "职位名称", requiredMode = Schema.RequiredMode.REQUIRED, example = "赵六")
    @NotEmpty(message = "职位名称不能为空")
    private String partyPostName;

    @Schema(description = "职位综述")
    private String partyPostDesc;

    @Schema(description = "岗位职责")
    private Set<Long> partyResponsebilityIds;

    @Schema(description = "岗位人员id", example = "13240")
    private Long partyUserId;

    @Schema(description = "职位人员名称", example = "芋艿")
    private String partyUserName;

}