package com.tom.party.module.party.dal.mysql.memberuserinfo;

import java.util.*;

import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.mybatis.core.query.LambdaQueryWrapperX;
import com.tom.party.framework.mybatis.core.mapper.BaseMapperX;
import com.tom.party.module.party.dal.dataobject.memberuserinfo.MemberUserInfoDO;
import org.apache.ibatis.annotations.Mapper;
import com.tom.party.module.party.controller.admin.memberuserinfo.vo.*;

/**
 * 党员信息 Mapper
 *
 * @author tom
 */
@Mapper
public interface MemberUserInfoMapper extends BaseMapperX<MemberUserInfoDO> {

    default PageResult<MemberUserInfoDO> selectPage(MemberUserInfoPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<MemberUserInfoDO>()
                .eqIfPresent(MemberUserInfoDO::getPartyBranch, reqVO.getPartyBranch())
                .eqIfPresent(MemberUserInfoDO::getMemberType, reqVO.getMemberType())
                .likeIfPresent(MemberUserInfoDO::getMemberName, reqVO.getMemberName())
                .eqIfPresent(MemberUserInfoDO::getSex, reqVO.getSex())
                .eqIfPresent(MemberUserInfoDO::getIdCard, reqVO.getIdCard())
                .eqIfPresent(MemberUserInfoDO::getNation, reqVO.getNation())
                .eqIfPresent(MemberUserInfoDO::getBirthday, reqVO.getBirthday())
                .eqIfPresent(MemberUserInfoDO::getEduLevel, reqVO.getEduLevel())
                .eqIfPresent(MemberUserInfoDO::getInPartyDay, reqVO.getInPartyDay())
                .eqIfPresent(MemberUserInfoDO::getRegularDay, reqVO.getRegularDay())
                .eqIfPresent(MemberUserInfoDO::getInWorkDay, reqVO.getInWorkDay())
                .eqIfPresent(MemberUserInfoDO::getPartyPostId, reqVO.getPartyPostId())
                .likeIfPresent(MemberUserInfoDO::getPartyPostName, reqVO.getPartyPostName())
                .likeIfPresent(MemberUserInfoDO::getWorkPostName, reqVO.getWorkPostName())
                .eqIfPresent(MemberUserInfoDO::getWorkPostLevel, reqVO.getWorkPostLevel())
                .eqIfPresent(MemberUserInfoDO::getUnit, reqVO.getUnit())
                .eqIfPresent(MemberUserInfoDO::getHabitation, reqVO.getHabitation())
                .eqIfPresent(MemberUserInfoDO::getPostLevel, reqVO.getPostLevel())
                .eqIfPresent(MemberUserInfoDO::getPartyGroup, reqVO.getPartyGroup())
                .eqIfPresent(MemberUserInfoDO::getStatus, reqVO.getStatus())
                .betweenIfPresent(MemberUserInfoDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(MemberUserInfoDO::getId));
    }

   default MemberUserInfoDO selectByName(String memberName) {
        return selectOne(MemberUserInfoDO::getMemberName,memberName);
   }
}