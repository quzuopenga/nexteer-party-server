package com.tom.party.module.party.framework.web.config;

import com.tom.party.framework.swagger.config.PartySwaggerAutoConfiguration;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class PartyWebConfiguration {

    /**
     * party 模块的 API 分组
     */
    @Bean
    public GroupedOpenApi partyGroupedOpenApi() {
        return PartySwaggerAutoConfiguration.buildGroupedOpenApi("party");
    }

}