package com.tom.party.module.party.service.memberuserinfo;

import com.tom.party.module.party.dal.dataobject.post.PartyPostDO;
import com.tom.party.module.party.dal.mysql.post.PartyPostMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import com.tom.party.module.party.controller.admin.memberuserinfo.vo.*;
import com.tom.party.module.party.dal.dataobject.memberuserinfo.MemberUserInfoDO;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.PageParam;
import com.tom.party.framework.common.util.object.BeanUtils;

import com.tom.party.module.party.dal.mysql.memberuserinfo.MemberUserInfoMapper;

import static com.tom.party.framework.common.exception.util.ServiceExceptionUtil.exception;
import static com.tom.party.module.party.enums.ErrorCodeConstants.*;

/**
 * 党员信息 Service 实现类
 *
 * @author tom
 */
@Service
@Validated
public class MemberUserInfoServiceImpl implements MemberUserInfoService {

    @Resource
    private MemberUserInfoMapper memberUserInfoMapper;

    @Resource
    private PartyPostMapper partyPostMapper;

    @Override
    @Transactional
    public Long createMemberUserInfo(MemberUserInfoSaveReqVO createReqVO) {
        validateBeforeCreate(createReqVO);

        // 插入
        MemberUserInfoDO memberUserInfo = BeanUtils.toBean(createReqVO, MemberUserInfoDO.class);
        memberUserInfoMapper.insert(memberUserInfo);

        if (createReqVO.getPartyPostId() != null) {
            PartyPostDO partyPostDO = new PartyPostDO();
            partyPostDO.setId(createReqVO.getPartyPostId());
            partyPostDO.setPartyUserId(memberUserInfo.getId());
            partyPostDO.setPartyUserName(memberUserInfo.getMemberName());
            partyPostMapper.updateById(partyPostDO);
        }

        // 返回
        return memberUserInfo.getId();
    }

    private void validateBeforeCreate(MemberUserInfoSaveReqVO createReqVO) {
        MemberUserInfoDO userInfoDO = memberUserInfoMapper.selectByName(createReqVO.getMemberName());
        if (userInfoDO != null) {
            throw exception(MEMBER_USER_INFO_EXISTS);
        }
    }

    @Override
    public void updateMemberUserInfo(MemberUserInfoSaveReqVO updateReqVO) {
        // 校验存在
        validateMemberUserInfoExists(updateReqVO.getId());
        // 更新
        MemberUserInfoDO updateObj = BeanUtils.toBean(updateReqVO, MemberUserInfoDO.class);
        if (updateObj.getPartyPostId() != null) {
            PartyPostDO partyPostDO = new PartyPostDO();
            partyPostDO.setId(updateReqVO.getPartyPostId());
            partyPostDO.setPartyUserId(updateReqVO.getId());
            partyPostDO.setPartyUserName(updateReqVO.getMemberName());
            partyPostMapper.updateById(partyPostDO);
        }

        memberUserInfoMapper.updateById(updateObj);
    }

    @Override
    public void deleteMemberUserInfo(Long id) {
        // 校验存在
        validateMemberUserInfoExists(id);
        // 删除
        memberUserInfoMapper.deleteById(id);
    }

    private void validateMemberUserInfoExists(Long id) {
        if (memberUserInfoMapper.selectById(id) == null) {
            throw exception(MEMBER_USER_INFO_NOT_EXISTS);
        }
    }

    @Override
    public MemberUserInfoDO getMemberUserInfo(Long id) {
        return memberUserInfoMapper.selectById(id);
    }

    @Override
    public PageResult<MemberUserInfoDO> getMemberUserInfoPage(MemberUserInfoPageReqVO pageReqVO) {
        return memberUserInfoMapper.selectPage(pageReqVO);
    }

}