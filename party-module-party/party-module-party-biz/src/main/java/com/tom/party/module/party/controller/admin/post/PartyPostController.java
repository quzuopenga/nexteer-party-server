package com.tom.party.module.party.controller.admin.post;

import cn.hutool.core.collection.CollUtil;
import com.tom.party.module.party.dal.dataobject.post.PartyPostDO;
import com.tom.party.module.party.service.post.PartyPostService;
import com.tom.party.module.system.api.dict.DictDataApi;
import com.tom.party.module.system.api.dict.dto.DictDataRespDTO;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import com.tom.party.framework.common.pojo.PageParam;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.CommonResult;
import com.tom.party.framework.common.util.object.BeanUtils;
import static com.tom.party.framework.common.pojo.CommonResult.success;

import com.tom.party.framework.excel.core.util.ExcelUtils;

import com.tom.party.framework.operatelog.core.annotations.OperateLog;
import static com.tom.party.framework.operatelog.core.enums.OperateTypeEnum.*;
import static com.tom.party.module.party.enums.DictTypeConstants.DICT_TYPE_POST_RESPONSEBILITY;

import com.tom.party.module.party.controller.admin.post.vo.*;

@Tag(name = "管理后台 - 党员组织")
@RestController
@RequestMapping("/party/post")
@Validated
public class PartyPostController {

    @Resource
    private PartyPostService partyPostService;


    @Resource
    private DictDataApi dictDataApi;

    @PostMapping("/create")
    @Operation(summary = "创建党员组织")
    @PreAuthorize("@ss.hasPermission('party:post:create')")
    public CommonResult<Long> createPost(@Valid @RequestBody PostSaveReqVO createReqVO) {
        return success(partyPostService.createPost(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新党员组织")
    @PreAuthorize("@ss.hasPermission('party:post:update')")
    public CommonResult<Boolean> updatePost(@Valid @RequestBody PostSaveReqVO updateReqVO) {
        partyPostService.updatePost(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除党员组织")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('party:post:delete')")
    public CommonResult<Boolean> deletePost(@RequestParam("id") Long id) {
        partyPostService.deletePost(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得党员组织")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('party:post:query')")
    public CommonResult<PostRespVO> getPost(@RequestParam("id") Long id) {
        PartyPostDO post = partyPostService.getPost(id);
        handleResponsebility(post);
        return success(BeanUtils.toBean(post, PostRespVO.class));
    }

    @GetMapping("/page")
    @Operation(summary = "获得党员组织分页")
    @PreAuthorize("@ss.hasPermission('party:post:query')")
    public CommonResult<PageResult<PostRespVO>> getPostPage(@Valid PostPageReqVO pageReqVO) {
        PageResult<PartyPostDO> pageResult = partyPostService.getPostPage(pageReqVO);
        handleResponsebility(pageResult.getList());
        return success(BeanUtils.toBean(pageResult, PostRespVO.class));
    }


    @GetMapping("/list")
    @Operation(summary = "获取树状")
    public CommonResult<List<PostRespVO>> getPostList(@Valid PostPageReqVO pageReqVO) {
        List<PartyPostDO> list = partyPostService.getPostList(pageReqVO);
        handleResponsebility(list);
        return success(BeanUtils.toBean(list, PostRespVO.class));
    }

    private void handleResponsebility(List<PartyPostDO> list) {
        for (PartyPostDO partyPostDO : list) {
            handleResponsebility(partyPostDO);
        }
    }

    private void handleResponsebility(PartyPostDO partyPostDO) {
        Set<Long> partyResponsebilityIds = partyPostDO.getPartyResponsebilityIds();
        if (CollUtil.isNotEmpty(partyResponsebilityIds)) {
            StringJoiner sj = new StringJoiner(",");
            for (Long id : partyResponsebilityIds) {
                DictDataRespDTO resp  = dictDataApi.getDictData(DICT_TYPE_POST_RESPONSEBILITY,id + "");
                String label = resp.getLabel();
                sj.add(label);
            }
            partyPostDO.setPartyResponsebility(sj.toString());
        }
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出党员组织 Excel")
    @PreAuthorize("@ss.hasPermission('party:post:export')")
    @OperateLog(type = EXPORT)
    public void exportPostExcel(@Valid PostPageReqVO pageReqVO,
              HttpServletResponse response) throws IOException {
        pageReqVO.setPageSize(PageParam.PAGE_SIZE_NONE);
        List<PartyPostDO> list = partyPostService.getPostPage(pageReqVO).getList();
        handleResponsebility(list);
        // 导出 Excel
        ExcelUtils.write(response, "党员组织.xls", "数据", PostRespVO.class,
                        BeanUtils.toBean(list, PostRespVO.class));
    }

}