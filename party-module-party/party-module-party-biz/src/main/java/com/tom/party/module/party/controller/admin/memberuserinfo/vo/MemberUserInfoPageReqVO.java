package com.tom.party.module.party.controller.admin.memberuserinfo.vo;

import lombok.*;

import java.time.LocalDate;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import com.tom.party.framework.common.pojo.PageParam;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;

import static com.tom.party.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

@Schema(description = "管理后台 - 党员信息分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MemberUserInfoPageReqVO extends PageParam {

    @Schema(description = "党支部")
    private Integer partyBranch;

    @Schema(description = "党员类别", example = "1")
    private Integer memberType;

    @Schema(description = "姓名", example = "芋艿")
    private String memberName;

    @Schema(description = "性别")
    private Integer sex;

    @Schema(description = "身份证")
    private String idCard;

    @Schema(description = "民族")
    private Integer nation;

    @Schema(description = "生日")
    private LocalDate birthday;

    @Schema(description = "教育水平")
    private Integer eduLevel;

    @Schema(description = "入党时间")
    private LocalDate inPartyDay;

    @Schema(description = "转正时间")
    private LocalDate regularDay;

    @Schema(description = "入厂时间")
    private LocalDate inWorkDay;

    @Schema(description = "职位id", example = "5634")
    private Long partyPostId;

    @Schema(description = "职位id", example = "芋艿")
    private String partyPostName;

    @Schema(description = "工作岗位", example = "芋艿")
    private String workPostName;

    @Schema(description = "工作职级")
    private Integer workPostLevel;

    @Schema(description = "所在单位")
    private Integer unit;

    @Schema(description = "党员所在基地")
    private String habitation;

    @Schema(description = "职务类别")
    private Integer postLevel;

    @Schema(description = "党小组")
    private Integer partyGroup;

    @Schema(description = "状态", example = "1")
    private Integer status;

    @Schema(description = "创建时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] createTime;

}