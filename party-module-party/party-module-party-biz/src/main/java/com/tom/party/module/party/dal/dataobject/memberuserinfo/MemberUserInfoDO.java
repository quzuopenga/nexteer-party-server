package com.tom.party.module.party.dal.dataobject.memberuserinfo;

import lombok.*;

import java.time.LocalDate;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import com.tom.party.framework.mybatis.core.dataobject.BaseDO;

/**
 * 党员信息 DO
 *
 * @author tom
 */
@TableName("party_member_user_info")
@KeySequence("party_member_user_info_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberUserInfoDO extends BaseDO {

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 党支部
     */
    private Integer partyBranch;
    /**
     * 党员类别
     */
    private Integer memberType;
    /**
     * 姓名
     */
    private String memberName;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 身份证
     */
    private String idCard;
    /**
     * 民族
     */
    private Integer nation;
    /**
     * 生日
     */
    private LocalDate birthday;
    /**
     * 教育水平
     */
    private Integer eduLevel;
    /**
     * 入党时间
     */
    private LocalDate inPartyDay;
    /**
     * 转正时间
     */
    private LocalDate regularDay;
    /**
     * 入厂时间
     */
    private LocalDate inWorkDay;
    /**
     * 职位id
     */
    private Long partyPostId;
    /**
     * 职位id
     */
    private String partyPostName;
    /**
     * 工作岗位
     */
    private String workPostName;
    /**
     * 工作职级
     */
    private Integer workPostLevel;
    /**
     * 所在单位
     */
    private Integer unit;
    /**
     * 党员所在基地
     */
    private String habitation;
    /**
     * 职务类别
     */
    private Integer postLevel;
    /**
     * 党小组
     */
    private Integer partyGroup;
    /**
     * 状态
     */
    private Integer status;

}