package com.tom.party.module.party.controller.admin.memberuserinfo;

import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import com.tom.party.framework.common.pojo.PageParam;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.CommonResult;
import com.tom.party.framework.common.util.object.BeanUtils;
import static com.tom.party.framework.common.pojo.CommonResult.success;

import com.tom.party.framework.excel.core.util.ExcelUtils;

import com.tom.party.framework.operatelog.core.annotations.OperateLog;
import static com.tom.party.framework.operatelog.core.enums.OperateTypeEnum.*;

import com.tom.party.module.party.controller.admin.memberuserinfo.vo.*;
import com.tom.party.module.party.dal.dataobject.memberuserinfo.MemberUserInfoDO;
import com.tom.party.module.party.service.memberuserinfo.MemberUserInfoService;

@Tag(name = "管理后台 - 党员信息")
@RestController
@RequestMapping("/party/member-user-info")
@Validated
public class MemberUserInfoController {

    @Resource
    private MemberUserInfoService memberUserInfoService;

    @PostMapping("/create")
    @Operation(summary = "创建党员信息")
    @PreAuthorize("@ss.hasPermission('party:member-user-info:create')")
    public CommonResult<Long> createMemberUserInfo(@Valid @RequestBody MemberUserInfoSaveReqVO createReqVO) {
        return success(memberUserInfoService.createMemberUserInfo(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新党员信息")
    @PreAuthorize("@ss.hasPermission('party:member-user-info:update')")
    public CommonResult<Boolean> updateMemberUserInfo(@Valid @RequestBody MemberUserInfoSaveReqVO updateReqVO) {
        memberUserInfoService.updateMemberUserInfo(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除党员信息")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('party:member-user-info:delete')")
    public CommonResult<Boolean> deleteMemberUserInfo(@RequestParam("id") Long id) {
        memberUserInfoService.deleteMemberUserInfo(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得党员信息")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('party:member-user-info:query')")
    public CommonResult<MemberUserInfoRespVO> getMemberUserInfo(@RequestParam("id") Long id) {
        MemberUserInfoDO memberUserInfo = memberUserInfoService.getMemberUserInfo(id);
        return success(BeanUtils.toBean(memberUserInfo, MemberUserInfoRespVO.class));
    }

    @GetMapping("/page")
    @Operation(summary = "获得党员信息分页")
    @PreAuthorize("@ss.hasPermission('party:member-user-info:query')")
    public CommonResult<PageResult<MemberUserInfoRespVO>> getMemberUserInfoPage(@Valid MemberUserInfoPageReqVO pageReqVO) {
        PageResult<MemberUserInfoDO> pageResult = memberUserInfoService.getMemberUserInfoPage(pageReqVO);
        return success(BeanUtils.toBean(pageResult, MemberUserInfoRespVO.class));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出党员信息 Excel")
    @PreAuthorize("@ss.hasPermission('party:member-user-info:export')")
    @OperateLog(type = EXPORT)
    public void exportMemberUserInfoExcel(@Valid MemberUserInfoPageReqVO pageReqVO,
              HttpServletResponse response) throws IOException {
        pageReqVO.setPageSize(PageParam.PAGE_SIZE_NONE);
        List<MemberUserInfoDO> list = memberUserInfoService.getMemberUserInfoPage(pageReqVO).getList();
        // 导出 Excel
        ExcelUtils.write(response, "党员信息.xls", "数据", MemberUserInfoRespVO.class,
                        BeanUtils.toBean(list, MemberUserInfoRespVO.class));
    }

}