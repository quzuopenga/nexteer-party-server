package com.tom.party.module.party.controller.admin.memberuserinfo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDate;
import java.util.*;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 党员信息新增/修改 Request VO")
@Data
public class MemberUserInfoSaveReqVO {

    @Schema(description = "id", requiredMode = Schema.RequiredMode.REQUIRED, example = "21007")
    private Long id;

    @Schema(description = "党支部", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "党支部不能为空")
    private Integer partyBranch;

    @Schema(description = "党员类别", requiredMode = Schema.RequiredMode.REQUIRED, example = "1")
    @NotNull(message = "党员类别不能为空")
    private Integer memberType;

    @Schema(description = "姓名", requiredMode = Schema.RequiredMode.REQUIRED, example = "芋艿")
    @NotEmpty(message = "姓名不能为空")
    private String memberName;

    @Schema(description = "性别", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "性别不能为空")
    private Integer sex;

    @Schema(description = "身份证", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "身份证不能为空")
    private String idCard;

    @Schema(description = "民族", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "民族不能为空")
    private Integer nation;

    @Schema(description = "生日", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "生日不能为空")
    private LocalDate birthday;

    @Schema(description = "教育水平", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "教育水平不能为空")
    private Integer eduLevel;

    @Schema(description = "入党时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "入党时间不能为空")
    private LocalDate inPartyDay;

    @Schema(description = "转正时间")
    private LocalDate regularDay;

    @Schema(description = "入厂时间")
    private LocalDate inWorkDay;

    @Schema(description = "职位id", requiredMode = Schema.RequiredMode.REQUIRED, example = "5634")
    @NotNull(message = "职位id不能为空")
    private Long partyPostId;

    @Schema(description = "职位id", requiredMode = Schema.RequiredMode.REQUIRED, example = "芋艿")
    @NotEmpty(message = "职位名称不能为空")
    private String partyPostName;

    @Schema(description = "工作岗位", example = "芋艿")
    private String workPostName;

    @Schema(description = "工作职级", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "工作职级不能为空")
    private Integer workPostLevel;

    @Schema(description = "所在单位", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "所在单位不能为空")
    private Integer unit;

    @Schema(description = "党员所在基地")
    private String habitation;

    @Schema(description = "职务类别")
    private Integer postLevel;

    @Schema(description = "党小组")
    private Integer partyGroup;

    @Schema(description = "状态", example = "1")
    private Integer status;

}