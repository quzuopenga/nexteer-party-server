package com.tom.party.module.party.enums;

import com.tom.party.framework.common.exception.ErrorCode;

/**
 * party 错误码枚举类
 *
 * party 系统，使用 1-002-000-000 段
 */
public interface ErrorCodeConstants {

    ErrorCode CHILD_EXISTS = new ErrorCode(1_002_000_000,"存在子数据,不允许删除");


    ErrorCode MEMBER_USER_INFO_NOT_EXISTS = new ErrorCode(1_002_000_001,"党员数据不存在");

    ErrorCode MEMBER_USER_INFO_EXISTS = new ErrorCode(1_002_000_002,"党员数据已存在");


    ErrorCode POST_NOT_EXISTS = new ErrorCode(1_002_000_003,"岗位数据不存在");



}
