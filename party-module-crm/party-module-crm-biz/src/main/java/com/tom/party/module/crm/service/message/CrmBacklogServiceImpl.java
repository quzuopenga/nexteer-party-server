package com.tom.party.module.crm.service.message;

import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.module.crm.controller.admin.backlog.vo.CrmTodayCustomerPageReqVO;
import com.tom.party.module.crm.dal.dataobject.customer.CrmCustomerDO;
import com.tom.party.module.crm.dal.mysql.customer.CrmCustomerMapper;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;

/**
 * 待办消息 Service 实现类
 *
 * @author dhb52
 */
@Component
@Validated
public class CrmBacklogServiceImpl implements CrmBacklogService {

    @Resource
    private CrmCustomerMapper customerMapper;

    @Override
    public PageResult<CrmCustomerDO> getTodayCustomerPage(CrmTodayCustomerPageReqVO pageReqVO, Long userId) {
        return customerMapper.selectTodayCustomerPage(pageReqVO, userId);
    }

}
