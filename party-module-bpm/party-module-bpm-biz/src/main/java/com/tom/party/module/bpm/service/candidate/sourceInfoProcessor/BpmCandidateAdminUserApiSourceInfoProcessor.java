package com.tom.party.module.bpm.service.candidate.sourceInfoProcessor;

import com.tom.party.framework.common.util.collection.SetUtils;
import com.tom.party.module.bpm.controller.admin.candidate.vo.BpmTaskCandidateRuleVO;
import com.tom.party.module.bpm.enums.definition.BpmTaskAssignRuleTypeEnum;
import com.tom.party.module.bpm.service.candidate.BpmCandidateSourceInfo;
import com.tom.party.module.bpm.service.candidate.BpmCandidateSourceInfoProcessor;
import com.tom.party.module.system.api.user.AdminUserApi;
import org.flowable.engine.delegate.DelegateExecution;

import javax.annotation.Resource;
import java.util.Set;

public class BpmCandidateAdminUserApiSourceInfoProcessor implements BpmCandidateSourceInfoProcessor {
    @Resource
    private AdminUserApi api;

    @Override
    public Set<Integer> getSupportedTypes() {
        return SetUtils.asSet(BpmTaskAssignRuleTypeEnum.USER.getType());
    }

    @Override
    public void validRuleOptions(Integer type, Set<Long> options) {
        api.validateUserList(options);
    }

    @Override
    public Set<Long> doProcess(BpmCandidateSourceInfo request, BpmTaskCandidateRuleVO rule, DelegateExecution delegateExecution) {
        return rule.getOptions();
    }
}