package com.tom.party.module.bpm.service.candidate.sourceInfoProcessor;

import com.tom.party.framework.common.util.collection.SetUtils;
import com.tom.party.module.bpm.controller.admin.candidate.vo.BpmTaskCandidateRuleVO;
import com.tom.party.module.bpm.enums.definition.BpmTaskAssignRuleTypeEnum;
import com.tom.party.module.bpm.service.candidate.BpmCandidateSourceInfo;
import com.tom.party.module.bpm.service.candidate.BpmCandidateSourceInfoProcessor;
import com.tom.party.module.system.api.dept.DeptApi;
import com.tom.party.module.system.api.dept.dto.DeptRespDTO;
import com.tom.party.module.system.api.user.AdminUserApi;
import com.tom.party.module.system.api.user.dto.AdminUserRespDTO;
import org.flowable.engine.delegate.DelegateExecution;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.tom.party.framework.common.util.collection.CollectionUtils.convertSet;

public class BpmCandidateDeptApiSourceInfoProcessor implements BpmCandidateSourceInfoProcessor {
    @Resource
    private DeptApi api;
    @Resource
    private AdminUserApi adminUserApi;

    @Override
    public Set<Integer> getSupportedTypes() {
        return SetUtils.asSet(BpmTaskAssignRuleTypeEnum.DEPT_MEMBER.getType(),
                BpmTaskAssignRuleTypeEnum.DEPT_LEADER.getType());
    }

    @Override
    public void validRuleOptions(Integer type, Set<Long> options) {
        api.validateDeptList(options);
    }

    @Override
    public Set<Long> doProcess(BpmCandidateSourceInfo request, BpmTaskCandidateRuleVO rule, DelegateExecution delegateExecution) {
        if (Objects.equals(BpmTaskAssignRuleTypeEnum.DEPT_MEMBER.getType(), rule.getType())) {
            List<AdminUserRespDTO> users = adminUserApi.getUserListByDeptIds(rule.getOptions());
            return convertSet(users, AdminUserRespDTO::getId);
        } else if (Objects.equals(BpmTaskAssignRuleTypeEnum.DEPT_LEADER.getType(), rule.getType())) {
            List<DeptRespDTO> depts = api.getDeptList(rule.getOptions());
            return convertSet(depts, DeptRespDTO::getLeaderUserId);
        }
        return Collections.emptySet();
    }
}