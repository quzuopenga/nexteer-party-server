package com.tom.party.module.bpm.service.candidate.sourceInfoProcessor;

import com.tom.party.framework.common.util.collection.SetUtils;
import com.tom.party.module.bpm.controller.admin.candidate.vo.BpmTaskCandidateRuleVO;
import com.tom.party.module.bpm.enums.definition.BpmTaskAssignRuleTypeEnum;
import com.tom.party.module.bpm.service.candidate.BpmCandidateSourceInfo;
import com.tom.party.module.bpm.service.candidate.BpmCandidateSourceInfoProcessor;
import com.tom.party.module.system.api.permission.PermissionApi;
import com.tom.party.module.system.api.permission.RoleApi;
import org.flowable.engine.delegate.DelegateExecution;

import javax.annotation.Resource;
import java.util.Set;

public class BpmCandidateRoleApiSourceInfoProcessor implements BpmCandidateSourceInfoProcessor {
    @Resource
    private RoleApi api;

    @Resource
    private PermissionApi permissionApi;

    @Override
    public Set<Integer> getSupportedTypes() {
        return SetUtils.asSet(BpmTaskAssignRuleTypeEnum.ROLE.getType());
    }

    @Override
    public void validRuleOptions(Integer type, Set<Long> options) {
        api.validRoleList(options);
    }

    @Override
    public Set<Long> doProcess(BpmCandidateSourceInfo request, BpmTaskCandidateRuleVO rule, DelegateExecution delegateExecution) {
        return permissionApi.getUserRoleIdListByRoleIds(rule.getOptions());
    }

}