package com.tom.party.module.pay.convert.demo;

import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.module.pay.controller.admin.demo.vo.transfer.PayDemoTransferCreateReqVO;
import com.tom.party.module.pay.controller.admin.demo.vo.transfer.PayDemoTransferRespVO;
import com.tom.party.module.pay.dal.dataobject.demo.PayDemoTransferDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author jason
 */
@Mapper
public interface PayDemoTransferConvert {

    PayDemoTransferConvert INSTANCE = Mappers.getMapper(PayDemoTransferConvert.class);

    PayDemoTransferDO convert(PayDemoTransferCreateReqVO bean);

    PageResult<PayDemoTransferRespVO> convertPage(PageResult<PayDemoTransferDO> pageResult);
}
