package com.tom.party.module.system.dal.mysql.userpartyinfo;


import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.mybatis.core.query.LambdaQueryWrapperX;
import com.tom.party.framework.mybatis.core.mapper.BaseMapperX;
import com.tom.party.module.system.dal.dataobject.userpartyinfo.UserPartyInfoDO;
import org.apache.ibatis.annotations.Mapper;
import com.tom.party.module.system.controller.admin.userpartyinfo.vo.*;

/**
 * 党员信息 Mapper
 *
 * @author tom
 */
@Mapper
public interface UserPartyInfoMapper extends BaseMapperX<UserPartyInfoDO> {

    default PageResult<UserPartyInfoDO> selectPage(UserPartyInfoPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<UserPartyInfoDO>()
                .eqIfPresent(UserPartyInfoDO::getPartyBranch, reqVO.getPartyBranch())
                .eqIfPresent(UserPartyInfoDO::getPartyGroup, reqVO.getPartyGroup())
                .eqIfPresent(UserPartyInfoDO::getUserId, reqVO.getUserId())
                .likeIfPresent(UserPartyInfoDO::getUsername, reqVO.getUsername())
                .eqIfPresent(UserPartyInfoDO::getNation, reqVO.getNation())
                .betweenIfPresent(UserPartyInfoDO::getJoinTime, reqVO.getJoinTime())
                .betweenIfPresent(UserPartyInfoDO::getConversionTime, reqVO.getConversionTime())
                .betweenIfPresent(UserPartyInfoDO::getEntryTime, reqVO.getEntryTime())
                .eqIfPresent(UserPartyInfoDO::getPartyPost, reqVO.getPartyPost())
                .eqIfPresent(UserPartyInfoDO::getWorkPost, reqVO.getWorkPost())
                .eqIfPresent(UserPartyInfoDO::getStatus, reqVO.getStatus())
                .betweenIfPresent(UserPartyInfoDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(UserPartyInfoDO::getId));
    }

    default UserPartyInfoDO selectByUserId(Long userId) {
        return selectOne(UserPartyInfoDO::getUserId,userId);
    }

    default UserPartyInfoDO selectByUserIdAndNeId(Long userId, Long id) {
        return selectOne(new LambdaQueryWrapperX<UserPartyInfoDO>().eq(UserPartyInfoDO::getUserId,userId)
                .ne(UserPartyInfoDO::getId,id));
    }
}