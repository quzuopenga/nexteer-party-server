package com.tom.party.module.system.dal.dataobject.userpartyinfo;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import com.tom.party.framework.mybatis.core.dataobject.BaseDO;

/**
 * 党员信息 DO
 *
 * @author tom
 */
@TableName("system_user_party_info")
@KeySequence("system_user_party_info_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPartyInfoDO extends BaseDO {

    /**
     * 党员信息id
     */
    @TableId
    private Long id;
    /**
     * 党支部
     */
    private Integer partyBranch;
    /**
     * 党小组
     */
    private Integer partyGroup;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 民族
     */
    private Integer nation;
    /**
     * 入党时间
     */
    private LocalDateTime joinTime;
    /**
     * 转正时间
     */
    private LocalDateTime conversionTime;
    /**
     * 入职时间
     */
    private LocalDateTime entryTime;
    /**
     * 党内职务
     */
    private Integer partyPost;
    /**
     * 工作职务
     */
    private Integer workPost;
    /**
     * 状态
     */
    private Integer status;

}