package com.tom.party.module.system.controller.admin.userpartyinfo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import javax.validation.constraints.*;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;

@Schema(description = "管理后台 - 党员信息新增/修改 Request VO")
@Data
public class UserPartyInfoSaveReqVO {

    @Schema(description = "党员信息id", requiredMode = Schema.RequiredMode.REQUIRED, example = "16097")
    private Long id;

    @Schema(description = "党支部", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "党支部不能为空")
    private Integer partyBranch;

    @Schema(description = "党小组", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "党小组不能为空")
    private Integer partyGroup;

    @Schema(description = "用户id", requiredMode = Schema.RequiredMode.REQUIRED, example = "1862")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    @Schema(description = "用户名", requiredMode = Schema.RequiredMode.REQUIRED, example = "李四")
    @NotEmpty(message = "用户名不能为空")
    private String username;

    @Schema(description = "民族", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "民族不能为空")
    private Integer nation;

    @Schema(description = "入党时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "入党时间不能为空")
    private LocalDateTime joinTime;

    @Schema(description = "转正时间")
    private LocalDateTime conversionTime;

    @Schema(description = "入职时间")
    private LocalDateTime entryTime;

    @Schema(description = "党内职务")
    private Integer partyPost;

    @Schema(description = "工作职务")
    private Integer workPost;

    @Schema(description = "状态", example = "2")
    private Integer status;

}