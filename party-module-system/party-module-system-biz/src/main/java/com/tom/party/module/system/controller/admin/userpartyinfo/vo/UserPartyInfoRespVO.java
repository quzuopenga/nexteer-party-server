package com.tom.party.module.system.controller.admin.userpartyinfo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.util.*;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;
import com.alibaba.excel.annotation.*;

@Schema(description = "管理后台 - 党员信息 Response VO")
@Data
@ExcelIgnoreUnannotated
public class UserPartyInfoRespVO {

    @Schema(description = "党员信息id", requiredMode = Schema.RequiredMode.REQUIRED, example = "16097")
    @ExcelProperty("党员信息id")
    private Long id;

    @Schema(description = "党支部", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("党支部")
    private Integer partyBranch;

    @Schema(description = "党小组", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("党小组")
    private Integer partyGroup;

    @Schema(description = "用户id", requiredMode = Schema.RequiredMode.REQUIRED, example = "1862")
    @ExcelProperty("用户id")
    private Long userId;

    @Schema(description = "用户名", requiredMode = Schema.RequiredMode.REQUIRED, example = "李四")
    @ExcelProperty("用户名")
    private String username;

    @Schema(description = "民族", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("民族")
    private Integer nation;

    @Schema(description = "入党时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("入党时间")
    private LocalDateTime joinTime;

    @Schema(description = "转正时间")
    @ExcelProperty("转正时间")
    private LocalDateTime conversionTime;

    @Schema(description = "入职时间")
    @ExcelProperty("入职时间")
    private LocalDateTime entryTime;

    @Schema(description = "党内职务")
    @ExcelProperty("党内职务")
    private Integer partyPost;

    @Schema(description = "工作职务")
    @ExcelProperty("工作职务")
    private Integer workPost;

    @Schema(description = "状态", example = "2")
    @ExcelProperty("状态")
    private Integer status;

    @Schema(description = "创建时间", requiredMode = Schema.RequiredMode.REQUIRED)
    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}