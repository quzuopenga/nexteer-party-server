package com.tom.party.module.system.controller.admin.userpartyinfo;

import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import com.tom.party.framework.common.pojo.PageParam;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.CommonResult;
import com.tom.party.framework.common.util.object.BeanUtils;
import static com.tom.party.framework.common.pojo.CommonResult.success;

import com.tom.party.framework.excel.core.util.ExcelUtils;

import com.tom.party.framework.operatelog.core.annotations.OperateLog;
import static com.tom.party.framework.operatelog.core.enums.OperateTypeEnum.*;

import com.tom.party.module.system.controller.admin.userpartyinfo.vo.*;
import com.tom.party.module.system.dal.dataobject.userpartyinfo.UserPartyInfoDO;
import com.tom.party.module.system.service.userpartyinfo.UserPartyInfoService;

@Tag(name = "管理后台 - 党员信息")
@RestController
@RequestMapping("/system/user-party-info")
@Validated
public class UserPartyInfoController {

    @Resource
    private UserPartyInfoService userPartyInfoService;

    @PostMapping("/create")
    @Operation(summary = "创建党员信息")
    @PreAuthorize("@ss.hasPermission('system:user-party-info:create')")
    public CommonResult<Long> createUserPartyInfo(@Valid @RequestBody UserPartyInfoSaveReqVO createReqVO) {
        return success(userPartyInfoService.createUserPartyInfo(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新党员信息")
    @PreAuthorize("@ss.hasPermission('system:user-party-info:update')")
    public CommonResult<Boolean> updateUserPartyInfo(@Valid @RequestBody UserPartyInfoSaveReqVO updateReqVO) {
        userPartyInfoService.updateUserPartyInfo(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除党员信息")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('system:user-party-info:delete')")
    public CommonResult<Boolean> deleteUserPartyInfo(@RequestParam("id") Long id) {
        userPartyInfoService.deleteUserPartyInfo(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得党员信息")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('system:user-party-info:query')")
    public CommonResult<UserPartyInfoRespVO> getUserPartyInfo(@RequestParam("id") Long id) {
        UserPartyInfoDO userPartyInfo = userPartyInfoService.getUserPartyInfo(id);
        return success(BeanUtils.toBean(userPartyInfo, UserPartyInfoRespVO.class));
    }

    @GetMapping("/page")
    @Operation(summary = "获得党员信息分页")
    @PreAuthorize("@ss.hasPermission('system:user-party-info:query')")
    public CommonResult<PageResult<UserPartyInfoRespVO>> getUserPartyInfoPage(@Valid UserPartyInfoPageReqVO pageReqVO) {
        PageResult<UserPartyInfoDO> pageResult = userPartyInfoService.getUserPartyInfoPage(pageReqVO);
        return success(BeanUtils.toBean(pageResult, UserPartyInfoRespVO.class));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出党员信息 Excel")
    @PreAuthorize("@ss.hasPermission('system:user-party-info:export')")
    @OperateLog(type = EXPORT)
    public void exportUserPartyInfoExcel(@Valid UserPartyInfoPageReqVO pageReqVO,
              HttpServletResponse response) throws IOException {
        pageReqVO.setPageSize(PageParam.PAGE_SIZE_NONE);
        List<UserPartyInfoDO> list = userPartyInfoService.getUserPartyInfoPage(pageReqVO).getList();
        // 导出 Excel
        ExcelUtils.write(response, "党员信息.xls", "数据", UserPartyInfoRespVO.class,
                        BeanUtils.toBean(list, UserPartyInfoRespVO.class));
    }

}