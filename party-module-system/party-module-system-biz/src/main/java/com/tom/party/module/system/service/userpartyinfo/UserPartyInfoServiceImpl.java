package com.tom.party.module.system.service.userpartyinfo;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import com.tom.party.module.system.controller.admin.userpartyinfo.vo.*;
import com.tom.party.module.system.dal.dataobject.userpartyinfo.UserPartyInfoDO;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.PageParam;
import com.tom.party.framework.common.util.object.BeanUtils;

import com.tom.party.module.system.dal.mysql.userpartyinfo.UserPartyInfoMapper;

import static com.tom.party.framework.common.exception.util.ServiceExceptionUtil.exception;
import static com.tom.party.module.system.enums.ErrorCodeConstants.*;

/**
 * 党员信息 Service 实现类
 *
 * @author tom
 */
@Service
@Validated
public class UserPartyInfoServiceImpl implements UserPartyInfoService {

    @Resource
    private UserPartyInfoMapper userPartyInfoMapper;

    @Override
    public Long createUserPartyInfo(UserPartyInfoSaveReqVO createReqVO) {
        validateBeforeCreate(createReqVO);
        // 插入
        UserPartyInfoDO userPartyInfo = BeanUtils.toBean(createReqVO, UserPartyInfoDO.class);
        userPartyInfoMapper.insert(userPartyInfo);
        // 返回
        return userPartyInfo.getId();
    }

    private void validateBeforeCreate(UserPartyInfoSaveReqVO createReqVO) {
        UserPartyInfoDO userPartyInfoDO = userPartyInfoMapper.selectByUserId(createReqVO.getUserId());
        if (userPartyInfoDO != null) {
            throw exception(USER_PARTY_INFO_EXISTS,createReqVO.getUsername());
        }
    }

    @Override
    public void updateUserPartyInfo(UserPartyInfoSaveReqVO updateReqVO) {
        validateBeforeUpdate(updateReqVO);
        // 校验存在
        validateUserPartyInfoExists(updateReqVO.getId());
        // 更新
        UserPartyInfoDO updateObj = BeanUtils.toBean(updateReqVO, UserPartyInfoDO.class);
        userPartyInfoMapper.updateById(updateObj);
    }

    private void validateBeforeUpdate(UserPartyInfoSaveReqVO updateReqVO) {
        UserPartyInfoDO userPartyInfoDO = userPartyInfoMapper.selectByUserIdAndNeId(updateReqVO.getUserId(),updateReqVO.getId());
        if (userPartyInfoDO != null) {
            throw exception(USER_PARTY_INFO_EXISTS,updateReqVO.getUsername());
        }
    }

    @Override
    public void deleteUserPartyInfo(Long id) {
        // 校验存在
        validateUserPartyInfoExists(id);
        // 删除
        userPartyInfoMapper.deleteById(id);
    }

    private void validateUserPartyInfoExists(Long id) {
        if (userPartyInfoMapper.selectById(id) == null) {
            throw exception(USER_PARTY_INFO_NOT_EXISTS);
        }
    }

    @Override
    public UserPartyInfoDO getUserPartyInfo(Long id) {
        return userPartyInfoMapper.selectById(id);
    }

    @Override
    public PageResult<UserPartyInfoDO> getUserPartyInfoPage(UserPartyInfoPageReqVO pageReqVO) {
        return userPartyInfoMapper.selectPage(pageReqVO);
    }

}