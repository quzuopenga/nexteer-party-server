package com.tom.party.module.system.service.userpartyinfo;

import java.util.*;
import javax.validation.*;
import com.tom.party.module.system.controller.admin.userpartyinfo.vo.*;
import com.tom.party.module.system.dal.dataobject.userpartyinfo.UserPartyInfoDO;
import com.tom.party.framework.common.pojo.PageResult;
import com.tom.party.framework.common.pojo.PageParam;

/**
 * 党员信息 Service 接口
 *
 * @author tom
 */
public interface UserPartyInfoService {

    /**
     * 创建党员信息
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createUserPartyInfo(@Valid UserPartyInfoSaveReqVO createReqVO);

    /**
     * 更新党员信息
     *
     * @param updateReqVO 更新信息
     */
    void updateUserPartyInfo(@Valid UserPartyInfoSaveReqVO updateReqVO);

    /**
     * 删除党员信息
     *
     * @param id 编号
     */
    void deleteUserPartyInfo(Long id);

    /**
     * 获得党员信息
     *
     * @param id 编号
     * @return 党员信息
     */
    UserPartyInfoDO getUserPartyInfo(Long id);

    /**
     * 获得党员信息分页
     *
     * @param pageReqVO 分页查询
     * @return 党员信息分页
     */
    PageResult<UserPartyInfoDO> getUserPartyInfoPage(UserPartyInfoPageReqVO pageReqVO);

}