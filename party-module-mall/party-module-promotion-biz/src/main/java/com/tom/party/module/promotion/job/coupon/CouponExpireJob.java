package com.tom.party.module.promotion.job.coupon;

import cn.hutool.core.util.StrUtil;
import com.tom.party.framework.quartz.core.handler.JobHandler;
import com.tom.party.framework.tenant.core.job.TenantJob;
import com.tom.party.module.promotion.service.coupon.CouponService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

// TODO 芋艿：配置一个 Job
/**
 * 优惠券过期 Job
 *
 * @author owen
 */
@Component
public class CouponExpireJob implements JobHandler {

    @Resource
    private CouponService couponService;

    @Override
    @TenantJob
    public String execute(String param) {
        int count = couponService.expireCoupon();
        return StrUtil.format("过期优惠券 {} 个", count);
    }

}
