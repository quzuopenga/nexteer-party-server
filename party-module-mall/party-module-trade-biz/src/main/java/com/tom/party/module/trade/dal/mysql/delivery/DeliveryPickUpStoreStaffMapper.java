package com.tom.party.module.trade.dal.mysql.delivery;

import com.tom.party.framework.mybatis.core.mapper.BaseMapperX;
import com.tom.party.module.trade.dal.dataobject.delivery.DeliveryPickUpStoreStaffDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DeliveryPickUpStoreStaffMapper extends BaseMapperX<DeliveryPickUpStoreStaffDO> {

}




